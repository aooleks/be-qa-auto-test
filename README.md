# BE QA AUTO TEST ASSIGNMENT v1.6

BE QA AUTO TEST ASSIGNMENT

**Requirements (obligatory):**
  - Refactor the given project
  - Make it run correctly on Gitlab CI
  - Use BDD format: Cucumber/Gherkin
  - Required framework: Java Serenity + Maven

**You need to:**
  - Automate the endpoints inside a working CI/CD pipeline to test the service
  - Cleanup the project and implement it in the proper way
  - Cover at least 1 positive and 1 negative scenario
  - Write instructions in Readme file on how to install, run and write new tests
  - Briefly write in Readme what was refactored and why
  - Upload your project to Gitlab and make sure that CI/CD is configured
  - Set Html reporting tool with the test results of the pipeline test run
  - IMPORTANT!!! Provide us with a Gitlab link to your repository
Good Luck! 

# SOLUTION


## What was refactored and why?

The original scenario was refactored, and new scenarios were introduced to enhance the testing of
the product search functionality. The changes made include:
  - **Clearer purpose and expected behavior:** The original code lacked clarity in terms of its purpose  
    and the expected outcomes. The refactored scenarios provide clear steps that describe the user's 
    action of sending a GET request for searching a specific product and the expected behavior in response.
  - **Improved structure with Scenario Outline:** The refactored scenarios use the Scenario Outline 
    format, which produce more concise and readable feature files. It eliminates the necessity for
    repetitive steps and give us an easier parameterization of different products to search for.
  - **Explicit specification of expected outcomes:** The refactored scenarios include explicit assertions 
    for each step. For the "A successful search for available products" scenario, it expects 
    a status code of 200 and a response containing a list of products. In the 
    "Negative product search" scenario, it expects a status code of 404, a response containing the message
    "Not found," and the requested product mentioned in the search result details.
  - **Replacement of one old scenario with new ones:** As part of the refactoring process, one old scenario
    was replaced with new scenarios. This change gives more specific and focused testing, covering both
    positive and negative search scenarios.

As the result of the refactoring the original scenario and introducing the new scenarios, the feature file now has
clearer, more structured, and comprehensive tests for the product search functionality.

## How to install, run and write new tests

### Installation

1. Clone the project repository:
   ```shell
   git clone git@gitlab.com:aooleks/be-qa-auto-test.git
   ```

2. Navigate to the project directory:
   ```shell
   cd ~/be-qa-auto-test
   ```

3. Install project dependencies using Maven:
   ```shell
   mvn clean install
   ```

### Running the Tests

#### In intellij idea

1. Run tests with `TestRunner.java` using configuration `-Denvironment=qa`

   ![Intellij IDEA Run configuration][idea-run-configuration]

   [idea-run-configuration]: img/configuration.png

#### In CLI

1. Execute all tests:
   ```shell
   mvn clean verify -Denvironment=qa
   ```

2. Generate Serenity BDD test reports:
   ```shell
   mvn serenity:aggregate
   ```

3. Open the generated test reports:
   Open the `{PROJECT_ROOT}/target/site/serenity/index.html` HTML file in your web browser.
 
#### In GitLab CI
   For CI pipelines to work properly add the following gitlab environment variable. Navigate to
   Settings -> CI/CD -> Variables -> Add variable:

     key: CI_ENV 
     value: qa

   Each commit initiates a GitLab CI pipeline that generates autotest results, which can 
   be viewed on the GitLab tests tab.

   For example:

   ![alt text1][gitlab-report]

   [gitlab-report]: img/gitlab-report.png

   For the main branch, an extra step called report is included, which generates a Serenity report in HTML format.
   This report can be accessed either through a direct link in the logs or by clicking the Browse button in the job report,
   which leads to the path public/index.html

   For example:

   ![alt text1][serenity-report]

   [serenity-report]: img/serenity-report.png

### Writing New Tests

1. Navigate to the `src/test/resources/features` directory.

2. Create a new `.feature` file or modify an existing one.

3. Write the test scenarios using the Gherkin syntax.

4. Create step definitions in the corresponding package under `src/test/java/stepdefinitions/stepdefinitions`.

5. Use Serenity BDD and Cucumber annotations to map the steps to the step definitions.

6. Implement the necessary test logic and assertions within the step definition methods.

