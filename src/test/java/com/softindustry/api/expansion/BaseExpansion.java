package com.softindustry.api.expansion;

import com.softindustry.utils.EnvironmentUtil;
import io.restassured.response.Response;
import net.thucydides.core.util.EnvironmentVariables;

import static net.serenitybdd.rest.RestRequests.given;

abstract class BaseExpansion {
    protected EnvironmentUtil environmentUtil;
    protected final String domain;
    protected String endPoint;

    BaseExpansion(EnvironmentVariables environmentVariables) {
        environmentUtil = new EnvironmentUtil(environmentVariables);
        domain = environmentUtil.getVariable("domain");
        endPoint = getEndpoint();
    }

    abstract protected String getEndpoint();

    Response sendGeneralGetRequest() {
        return given()
                .when().get(domain + endPoint);
    }
}
