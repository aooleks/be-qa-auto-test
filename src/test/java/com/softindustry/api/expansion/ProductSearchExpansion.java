package com.softindustry.api.expansion;

import io.restassured.response.Response;
import net.thucydides.core.util.EnvironmentVariables;

import static net.serenitybdd.rest.SerenityRest.given;

public class ProductSearchExpansion extends BaseExpansion {

    public ProductSearchExpansion(EnvironmentVariables environmentVariables, String searchSegment) {
        super(environmentVariables);
        endPoint = endPoint.replace("{searchSegment}", searchSegment);
    }

    @Override
    protected String getEndpoint() {
        return environmentUtil.getVariable("api.product.search");
    }

    public Response searchForProduct(String productName) {
        return given().pathParam("product", productName).get(domain + endPoint);
    }
}
