package com.softindustry.api.tests;

import com.softindustry.api.expansion.ProductSearchExpansion;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.module.jsv.JsonSchemaValidator;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.util.EnvironmentVariables;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.is;

public class SearchStepDefinitions {

    private EnvironmentVariables environmentVariables;
    private ProductSearchExpansion productSearchExpansion;

    @Given("the user sets search segment to {string}")
    public void setTheProductAPIEndpoint(String searchSegment) {
        productSearchExpansion = new ProductSearchExpansion(environmentVariables, searchSegment);
    }

    @Then("the response should have status code {int}")
    public void theResponseShouldHaveStatusCode(int statusCode) {
        restAssuredThat(response -> response.statusCode(statusCode));
    }

    @When("the user sends a GET request for {string} searching")
    public void theUserSendsAGETRequestToEndpointWithSearchingProduct(String productName) {
        productSearchExpansion.searchForProduct(productName);
    }

    @And("the response should contain list of products")
    public void theResponseShouldContainListOfProducts() {
        restAssuredThat(response -> response.body(JsonSchemaValidator.
                matchesJsonSchemaInClasspath("product_schema.json"))
        );
    }

    @And("the response should contain {string} detail")
    public void theResponseShouldContainAnErrorMessageIndicatingAMissingOrInvalidSearchQuery(String detail) {
        restAssuredThat(response -> response.body("detail", is(detail)));
    }

    @And("the response should contain detail {string}")
    public void theResponseShouldContainDetailNotFound(String detail) {
        SerenityRest.restAssuredThat(response -> response.body("detail", is(detail)));
    }

    @And("the response should contain {string} message")
    public void theResponseShouldContainMessage(String message) {
        SerenityRest.restAssuredThat(response -> response.body("detail.message", is(message)));
    }

    @And("{string} requested_item in the search result details")
    public void productRequested_itemInTheSearchResultDetails(String product) {
        SerenityRest.restAssuredThat(response -> response.body("detail.requested_item", is(product)));
    }
}
