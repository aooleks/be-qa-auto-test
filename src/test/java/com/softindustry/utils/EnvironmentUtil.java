package com.softindustry.utils;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;

public class EnvironmentUtil {
    private final EnvironmentVariables environmentVariables;

    public EnvironmentUtil(EnvironmentVariables environmentVariables) {
        this.environmentVariables = environmentVariables;
    }

    public String getVariable(String property) {
        Injectors.getInjector().getInstance(EnvironmentVariables.class);
        return EnvironmentSpecificConfiguration.from(environmentVariables).getProperty(property);
    }
}
