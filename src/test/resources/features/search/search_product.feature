Feature: Search for the product

  Scenario Outline: A successful search for available product in proper segment 'demo' for <product>
    Given the user sets search segment to 'demo'
    When the user sends a GET request for '<product>' searching
    Then the response should have status code 200
    And the response should contain list of products

    Examples:
      | product |
      | orange  |
      | apple   |
      | pasta   |
      | cola    |

  Scenario Outline: Negative product search in not-existed segment 'test' for proper product <product>
    Given the user sets search segment to 'test'
    When the user sends a GET request for '<product>' searching
    Then the response should have status code 404
    And the response should contain detail 'Not Found'

    Examples:
      | product |
      | orange  |
      | apple   |
      | pasta   |
      | cola    |

  Scenario Outline: Negative product search in proper segment 'demo' for <product>
    Given the user sets search segment to 'demo'
    When the user sends a GET request for '<product>' searching
    Then the response should have status code 404
    And the response should contain 'Not found' message
    And '<product>' requested_item in the search result details

    Examples:
      | product |
      | mango   |
      | car     |

  Scenario Outline: Search with empty product in search segment <searchSegment>
    Given the user sets search segment to '<searchSegment>'
    When the user sends a GET request for '' searching
    Then the response should have status code 401
    And the response should contain 'Not authenticated' detail

    Examples:
      | searchSegment |
      | demo          |
      | test          |
